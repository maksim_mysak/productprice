<?php

use Illuminate\Database\Seeder;

class PriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!\App\Product::count()) {
            $this->call(ProductSeeder::class);
        }
        $products = \App\Product::all();

        factory(\App\Price::class, 130)->make()->each(function (\App\Price $m) use ($products) {
            $m->product()->associate($products->random());
            $m->save();
        });
    }
}