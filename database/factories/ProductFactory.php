<?php

use Faker\Generator as Faker;


$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'name' => $faker->numerify('\'Продукт «Школьная форма»\' ###'),
        'priority_type' => $faker->randomElement($array = array ('sort','low')),
        'amount' => $faker->randomElement($array = array (25,50,1000,10000, 89234, 7777)),
    ];
});