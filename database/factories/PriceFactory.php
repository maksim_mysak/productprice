<?php

use Faker\Generator as Faker;


$factory->define(App\Price::class, function (Faker $faker) {
    return [
        'amount' => $faker->randomElement($array = array (25,50,1000,10000, 89234, 7777)),
        'date_on' => $faker->dateTimeInInterval($startDate = '-30 years', $interval = '+ 5 days', $timezone = null),
        'date_from' =>$faker->dateTimeInInterval($startDate = '-10 years', $interval = '+ 5 days', $timezone = null),
    ];
});