## Installation Steps


### 1. git clone this repository

### 2. Add the DB Credentials & APP_URL

Next make sure to create a new database and add your database credentials to your .env file:

```
DB_HOST=localhost
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret
```

```
cd productprice
composer install
php artisan migrate
php artisan db:seed
```

