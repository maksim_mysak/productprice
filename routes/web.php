<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','ProductController@index');
Route::get('/product/new','ProductController@create');
Route::post('/product/new','ProductController@store');
Route::get('/product/{id}/edit','ProductController@edit');
Route::post('/product/{id}/edit','ProductController@update');
Route::get('/chart','ProductController@chart');



