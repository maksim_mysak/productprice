<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Price;
use DB;

/**
 * Class Product
 * @package App
 */
class Product extends Model
{

    /**
     * @var array
     */
    protected $fillable = [
        'amount'
    ];

    /**
     * @param $data
     * @return int
     */
    public function saveProduct($data): int
    {
        $this->name = $data['name'];
        $this->amount = $data['amount'];
        $this->save();

        return 1;
    }

    /**
     * @param $data
     * @return int
     */
    public function updateProduct($data): int
    {
        $product = $this->find($data['id']);
        $this->name = $data['name'];
        $this->amount = $data['amount'];
        $this->priority_type = $data['priority_type'];
        $product->save();

        if( !empty($data['new_price'])) {
            foreach ($data['new_price'] as $item) {
                $price = new Price();
                $price->amount = $item['amount'];
                $price->date_on = $item['date_on'];
                $price->date_from = $item['date_from'];
                $price->products_id = $product->id;
                $price->save();
            }
        }

        if( !empty($data['prices'])) {
            foreach ($data['prices'] as $k => $item) {
                $price = Price::find($k);
                $price->amount = $item['amount'];
                $price->date_on = $item['date_on'];
                $price->date_from = $item['date_from'];
                $price->save();
            }
        }
        return 1;
    }

    /**
     * @return mixed
     */
    public function getAmountAttribute()
    {
        return $this->price($this->priority_type);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function prices()
    {
        return $this->HasMany(\App\Price::class, 'products_id', 'id');
    }

    /**
     * @param $type
     * @return mixed
     */
    public function price($type)
    {
        if ($type == 'sort') {
            $resPrice = DB::table('prices')
                ->select('amount', 'date_on', 'date_from')
                ->where('products_id', '=', $this->id)
                ->whereRaw('DATE(date_on) <= DATE(CURRENT_DATE)' )
                ->whereRaw('DATE(date_from) >=  DATE(CURRENT_DATE)' )
                ->orderBy('date_from', 'desc')
                ->first();
            if ($resPrice && $resPrice->amount > 0) {
                return $resPrice->amount;
            }

        } elseif($type == 'low') {

            $resPrice = DB::table('prices')
                ->select('amount', 'date_on', 'date_from')
                ->addSelect(DB::raw('(DATE(date_from)-DATE(date_on)) AS period'))
                ->where('products_id', '=', $this->id)
                ->whereRaw('DATE(date_on) <= DATE(CURRENT_DATE)' )
                ->whereRaw('DATE(date_from) >=  DATE(CURRENT_DATE)' )
                ->orderBy('period', 'asc')
                ->first();
            if ($resPrice && $resPrice->amount > 0) {
                return $resPrice->amount;
            }
        }

        return $this->attributes['amount'];

    }
}
