<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Price
 * @package App
 */
class Price extends Model
{
    /**
     * @var string
     */
    protected $table = 'prices';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(\App\Product::class, 'products_id');
    }
}
