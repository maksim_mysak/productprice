<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Sesion;
use DB;
/**
 * Class ProductController
 * @package App\Http\Controllers
 */
class ProductController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $products = Product::paginate(15);
        return view('product.index', ['products' => $products]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('product.create');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $product = new Product();
        $data = $this->validate($request, [
            'name' => 'required',
            'amount' => 'required',
        ]);

        $product->saveProduct($data);
        return redirect('/')->with('success', 'New product has been created!');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $product = Product::find($id);

        return view('product.edit', compact('product', 'id'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $castomer = new Product();


        $data = $this->validate($request, [
            'name' => 'required',
            'amount' => 'required',
            'priority_type' => 'required',
        ]);

        if ($request->has('prices')) {
            $data['prices'] = $request->input('prices');
        }

        if ($request->has('new_price')) {
            $data['new_price'] = $request->input('new_price');
        }

        $data['id'] = $id;
        $castomer->updateProduct($data);

        return redirect('/')->with('success', 'Product has been updated!');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function chart()
    {
        $products = Product::all();

        return view('product.chart', [
            'products' => $products,
        ]);
    }
}
