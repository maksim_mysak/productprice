@extends('main')

@section('content')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
    <div class="container">
        <div class="row">
            <canvas id=myChart width=900 height=360></canvas>
        </div>
    </div>
    <script>
        var ctx = document.getElementById("myChart").getContext('2d');
        var datasets = [{
            "label": "Приоритетнее цена, установленная позднее (используя сортировку)",
            "borderWidth": 1,
            "backgroundColor": "#007bff",
            "data": [
                @foreach($products as $price)
                {
                    "x": "{{$price->id}}",
                    "y": "{{$price->price('sort')}}"
                },
                @endforeach
            ]
        },  {
            "label": "Приоритетнее цена с меньшим периодом действия (цена на 1 месяц приоритетнее цены установленной на 1 год)",
            "borderWidth": 1,
            "backgroundColor": "#28a745",
            "data": [
            @foreach($products as $price)

            {
                "x": "{{$price->id}}",
                "y": "{{$price->price('low')}}"
            },
                @endforeach
            ]
        }];
        var color = Chart.helpers.color;
        for (var index_k in datasets) {
            datasets[index_k].backgroundColor = color(datasets[index_k].backgroundColor).alpha(0.3).rgbString();
        }
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: [
                    @foreach($products as $price) "{{$price->id}}", @endforeach
                ],
                datasets: datasets
            },
            options: {
                responsive: false,
                scales: {
                    yAxes: [{
                        ticks: {}
                    }]
                }
            }
        });</script>
@endsection