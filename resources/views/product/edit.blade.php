@extends('main')

@section('content')
    <div class="container">
        <h1>new Product</h1>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
        @endif

        <div class="row">
            <div class="col-md-8 order-md-1">
                <form method="post" action="{{url('/product/' . $id . '/edit')}}">
                    <div class="form-group">
                        <input type="hidden" value="{{csrf_token()}}" name="_token" />
                        <label for="title">Name</label>
                        <input type="text" class="form-control" value="{{$product->name}}" name="name" required/>
                    </div>

                    <div class="form-group">
                        <label for="title">default Price</label>
                        <input type="number" class="form-control" value="{{$product->amount}}" name="amount" required/>
                    </div>

                    <div class="form-group">
                        <label for="title">Type</label>
                        <select name="priority_type" class="form-control" required>
                            <option value=""></option>
                            <option {{ ($product->priority_type == 'low') ? "selected=selected" : ""}}
                                    value="low"
                                    title="Приоритетнее цена с меньшим периодом действия (цена на 1 месяц
                                     приоритетнее цены установленной на 1 год)">
                                Приоритетнее цена с меньшим периодом действия
                            </option>
                            <option {{($product->priority_type == 'sort') ? "selected=selected" : ""}}
                                    title="Приоритетнее цена, установленная позднее (используя сортировку)"
                                    value="sort">Приоритетнее цена, установленная позднее
                            </option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="title">Price Matrix</label>
                        <div id="price_matrix" class="form-group">
                            <div class="row">
                                <div class="col-3">price</div>
                                <div class="col-4">В период с</div>
                                <div class="col-3">В период по</div>
                            </div>
                            @if(count($product->prices))
                                @foreach($product->prices as $price)
                                    <div class="row">
                                        <input type="number"
                                               name="prices[{{$price->id}}][amount]"
                                               value="{{$price->amount}}"
                                               class="form-control col-3"
                                               required/>
                                        <input type="date"
                                               name="prices[{{$price->id}}][date_on]"
                                               class="form-control col-4"
                                               value="{{ \Carbon\Carbon::parse($price->date_on)->format('Y-m-d') }}"
                                               required/>
                                        <input type="date"
                                               name="prices[{{$price->id}}][date_from]"
                                               class="form-control col-3"
                                               value="{{ \Carbon\Carbon::parse($price->date_from)->format('Y-m-d') }}"
                                               required/>
                                        @if ($loop->last)
                                            <button type="button" class="btn btn-primary _add_row">+</button>
                                        @endif
                                    </div>
                                @endforeach
                            @else
                                <button type="button" class="btn btn-primary _add_row">+</button>
                            @endif
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary">Updete</button>
                </form>
            </div>
        </div>
    </div>
    <script>
        window.indexi = 1;
        $(document).on('click', '._add_row', function () {
            var i = window.indexi;
            window.indexi++;
            $('#price_matrix').append('<div class="row">' +
                    '<input type="number" name="new_price[' + i + '][amount]" class="form-control col-3" required/>' +
                    '<input type="date" name="new_price[' + i + '][date_on]" class="form-control col-4" required/>' +
                    '<input type="date" name="new_price[' + i + '][date_from]" class="form-control col-3" required/>' +
                    '<button type="button" class="btn btn-primary _delete_row">-</button>' +
                    '</div>');
        });
        $(document).on('click', '._delete_row', function () {
            $(this).parent().remove();
        });

    </script>

    <style>
        #price_matrix .row input{
            margin: 2%;
        }
    </style>
@endsection

