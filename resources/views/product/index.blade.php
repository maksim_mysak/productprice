@extends('main')

@section('content')
    <div class="container">
        <h1>Products <a href="{{url('/product/new')}}" class="btn btn-primary">create</a></h1>

        <p><a href="{{url('/chart')}}">chart price</a></p>
        <table class="table table-striped">
            <thead>
            <tr>
                <td>#id</td>
                <td>Name</td>
                <td>Price</td>
                <td>Action</td>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product)
                <tr>
                    <td>{{$product->id}}</td>
                    <td>{{$product->name}}</td>
                    <td>{{$product->amount}}</td>
                    <td>
                        <a class="btn btn-primary"
                           href="{{url('/product/' . $product->id. '/edit')}}"
                           role="button">Edit</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div>

        <div class="row">{{ $products->links() }}</div>
@endsection